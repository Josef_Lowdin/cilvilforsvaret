package se.lowdin.civilforsvaret.webapp;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Blob;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotEmpty;


@Entity
public class Person {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID")
	private int id;
	@Column(name = "FIRSTNAME")
	private String firstName;
	@Column(name = "SECONDNAME")
	private String lastName;
	@Column(name = "EMAIL")
	private String email;
	@Column(name = "MobilePHONENUM")
	private String phoneNumber;
	@Column(name = "OTHER")
	private String otherInfo;
	@Column(name = "image")
	@Lob
	private Blob image;
	@NotEmpty
	@Size(min = 1, max = 50)
	@Column(name = "username")
	private String userName;
	@NotEmpty
	@Size(min = 1, max = 20)
	@Column(name = "password")
	private String password;

	public Person() {

	}

	public Person(int id, String firstName, String lastName, String email,
			String phoneNumber, String otherInfo, Blob image, String userName, String password) {

		setId(id);
		setFirstName(firstName);
		setLastName(lastName);
		setEmail(email);
		setPhoneNumber(phoneNumber);
		setOtherInfo(otherInfo);
		setImage(image);
		setUserName(password);
		setPassword(userName);

	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getOtherInfo() {
		return otherInfo;
	}

	public void setOtherInfo(String other) {
		this.otherInfo = other;
	}

	public long getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	public Blob getImage() {
		
		return image;
		
	}
	
	public void setImage(Blob image){
		
		this.image = image;
		
	}
	
	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getUserName() {
		return userName;
	}

	public void setPassword(String password) {
/***				         
		        String md5 = null;
		         
		        try {
		             
		        //Create MessageDigest object for MD5
		        MessageDigest digest = MessageDigest.getInstance("MD5");
		         
		        //Update input string in message digest
		        digest.update(password.getBytes(), 0, password.length());
		 
		        //Converts message digest value in base 16 (hex) 
		        md5 = new BigInteger(1, digest.digest()).toString(16);
		 
		        } catch (NoSuchAlgorithmException e) {
		 
		            e.printStackTrace();
		        }
		     
		this.password = md5;
		***/
		this.password = password;
	}

	public String getPassword() {
		return password;
	}

}