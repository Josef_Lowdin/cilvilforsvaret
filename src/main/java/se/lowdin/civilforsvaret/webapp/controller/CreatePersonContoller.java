package se.lowdin.civilforsvaret.webapp.controller;

import java.io.IOException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Blob;
import java.util.Map; 
import java.util.logging.Logger;

import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import se.lowdin.civilforsvaret.webapp.Person;
import se.lowdin.civilforsvaret.webapp.service.PersonService;
	
	@Controller
	public class CreatePersonContoller {
		
		@Autowired
		PersonService service;
		
	
		/***
		 * Prepare the site to get all the person data by creating a new instance.
		 * @param map
		 * @return "html page"
		 */
		@RequestMapping("/createPerson.html")
		    public String createPerson(Map<String, Object> map) {
		       
			try {
		            map.put("person", new Person());
		        } catch(Exception e) {
		            e.printStackTrace();
		        }
		 
		        return "newPerson";
		    }
		 
		/***
		 * Save the new data into a database..
		 * @param person
		 * @param file
		 * @return "redirect:/html page"
		 */
		    @RequestMapping(value = "/save.html", method = RequestMethod.POST)
		    public String save(
		            @ModelAttribute("person") Person person,
		            @RequestParam(value = "file", required = false) MultipartFile file) {
		         

		        	try {
		        		
						Blob blob = Hibernate.createBlob(file.getInputStream());
						person.setImage(blob);
			            service.createPerson(person);
			            
					} catch (IOException e) {
						
						e.printStackTrace();
					}
		      
		           
		        return "redirect:/person.html";
		    }
		 
		
}
