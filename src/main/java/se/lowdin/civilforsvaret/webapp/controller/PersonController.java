package se.lowdin.civilforsvaret.webapp.controller;


import java.io.IOException;
import java.io.OutputStream;
import java.sql.Blob;
import java.sql.SQLException;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;
import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import se.lowdin.civilforsvaret.webapp.Person;
import se.lowdin.civilforsvaret.webapp.service.PersonService;

@Controller
public class PersonController {
	
	@Autowired
	private PersonService service;

	Logger log = Logger.getRootLogger();
	
	/***
	 * 
	 * @param map
	 * @return "html page"
	 */
	@RequestMapping("/person.html")
	public String setupForm(Map<String, Object> map) {

		Person person = new Person();
		map.put("person", person);
		map.put("personList", service.getAllPersons());
		return "person";
	}

	/***
	 * Delete a person from the database
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/delete/{id}", method = RequestMethod.GET)
	public ModelAndView deletePerson(@PathVariable Integer id) {
		ModelAndView mav = new ModelAndView("redirect:/person.html");
		service.deletePerson(id);
		return mav;
	}

	/***
	 * Upload the images from the database.
	 * @param id
	 * @param response
	 * @return 
	 * @throws SQLException
	 */
	@RequestMapping(value = "/image/{id}", method = RequestMethod.GET)
	public void viewImages(@PathVariable int id, HttpServletResponse response) throws SQLException {

		 Person person = service.getPerson(id);
		 
		 try {
            
            OutputStream out = response.getOutputStream();
            response.setContentType("image/JPEG");
            IOUtils.copy(person.getImage().getBinaryStream(), out);
            out.flush();
            out.close();
         
        } catch (IOException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
         
     
    }
	
}
