package se.lowdin.civilforsvaret.webapp.controller;

import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.apache.commons.codec.binary.Hex;
import org.apache.commons.codec.digest.Md5Crypt;

import se.lowdin.civilforsvaret.webapp.Person;
import se.lowdin.civilforsvaret.webapp.service.PersonService;

@Controller
@RequestMapping("/index.html")
public class LoginController {
	
	boolean success = false;
	
	@Autowired
	PersonService service;
	
	/***
	 * Prepare the website to get all the peron data by creating a new instance.
	 * 
	 * @param model
	 * @return "html page"
	 */
	
	@RequestMapping(method = RequestMethod.GET)
	public String showForm(Map model) {
		Person person = new Person();
		model.put("loginForm", person);	
		return "index";
	}
	
	/***
	 * 
	 * Process your login by getting the data from the RequestParam in the .jsp page.
	 * 
	 * @param user
	 * @param result
	 * @param username
	 * @param password
	 * @return "html page"
	 */

	@RequestMapping(method = RequestMethod.POST)
	public String processLogin(Person user, BindingResult result, 
			 @RequestParam("userName")String username, @RequestParam("password")String password) {
			
	
		ValidateUser(username, password);
	
		
		String destination = "";
		
		
		if (success == true){
			
			destination ="redirect:/person.html";
			
		}
		
		else {
			
			destination ="redirect:/index.html";
			
		}
		
		return destination;
	}
	
	/***
	 * 
	 * Validates the username and password with the database
	 * 
	 * @param username
	 * @param password
	 * @return boolean
	 */
	
	public boolean ValidateUser(String username, String password) {

		
		List<Person> users = service.getAllPersons();

		for (Person allUsers : users) {
          
			if (allUsers.getUserName().equals(username)
					&& allUsers.getPassword().equals(password)){

			success = true;
			
			}
	

		}

		return success;

	}
}