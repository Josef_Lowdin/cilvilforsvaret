package se.lowdin.civilforsvaret.webapp.controller;


import java.io.IOException;
import java.sql.Blob;
import java.util.Map;

import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import se.lowdin.civilforsvaret.webapp.Person;
import se.lowdin.civilforsvaret.webapp.beans.EditPersonBean;
import se.lowdin.civilforsvaret.webapp.service.PersonService;

@Controller

@RequestMapping("/editPerson/{id}.html")
public class EditPersonContoller {
	
	private int personId;
	
	@Autowired
	PersonService service;

		/***
		 * 
		 * @param id
		 * @return ModelAndView 
		 */
	@RequestMapping(method = RequestMethod.GET)
	public ModelAndView index(@PathVariable int id) {

		setPersonId(id);
		EditPersonBean bean = new EditPersonBean();

		if (id > 0) {
			//Get the person from database
			Person person = service.getPerson(id);
			//Copy values to bean
			bean.copyValuesToBean(person);
		}

		ModelAndView mav = new ModelAndView("editPerson");
		//Add bean values into the ModelAndView
		mav.addObject("editPersonBean", bean);
		return mav;

	}
	
	/***
	 * 
	 * @param person
	 * @param file
	 * @return "redirect:/html page"
	 */
	 @RequestMapping(value = "/edit.html", method = RequestMethod.POST)
	    public String save(
	            @ModelAttribute("editPersonBean") Person person,
	            @RequestParam(value = "file", required = false) MultipartFile file) {
	         
		         deletePerson(getPersonId());

	        	try {
	        		
	        		//Get the inputstream of the file and stor it as a Blob
					Blob blob = Hibernate.createBlob(file.getInputStream());
					person.setImage(blob);
		            service.createPerson(person);
		            
				} catch (IOException e) {
					
					e.printStackTrace();
				}
	      
	           
	        return "redirect:/person.html";
	    }
	 
	    
	    public void deletePerson(int id) {
			
			service.deletePerson(id);
	
		}
		
	    public int getPersonId() {
			return personId;
		}
		public void setPersonId(int personId) {
			this.personId = personId;
		}
	    
}
