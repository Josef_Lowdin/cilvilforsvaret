package se.lowdin.civilforsvaret.webapp.service.impl;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import se.lowdin.civilforsvaret.webapp.Person;
import se.lowdin.civilforsvaret.webapp.repository.PersonRepository;
import se.lowdin.civilforsvaret.webapp.service.PersonService;


@Service
public class PersonServiceImpl implements PersonService {
	
	Logger log = Logger.getLogger(PersonServiceImpl.class);
	
	@Autowired
	private PersonRepository personRepository;

	@Transactional
	public void createPerson(Person person) {
		
		 personRepository.createPerson(person); 
		
	}
	@Transactional
	public void deletePerson(int id) {
		
		personRepository.deletePerson(id);
		
	}
	@Transactional
	public void updatePerson(Person person) {
		
		personRepository.editPerson(person);
		
	}
	@Transactional
	public List<Person> getAllPersons() {
		
		return personRepository.getAllPersons();
	}
	@Transactional
	public Person getPerson(int id) {
		
		return personRepository.getPerson(id);
	}
	

	public void setPersonRepository(PersonRepository personRepository) {

      this.personRepository = personRepository;
		
	}



}
