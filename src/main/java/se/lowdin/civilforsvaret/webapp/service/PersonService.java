package se.lowdin.civilforsvaret.webapp.service;

import java.util.List;

import se.lowdin.civilforsvaret.webapp.Person;



public interface PersonService {
	
    void createPerson(Person person);
	
	void deletePerson(int id);
	
	void updatePerson(Person person);
	
	List<Person>getAllPersons();
	
	Person getPerson(int id);
}
