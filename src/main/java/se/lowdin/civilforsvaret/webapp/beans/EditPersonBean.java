package se.lowdin.civilforsvaret.webapp.beans;


import java.sql.Blob;

import se.lowdin.civilforsvaret.webapp.Person;


	public class EditPersonBean {

		private int id;
		private String firstName;
		private String lastName;
		private String email;
		private String phoneNumber;
		private String otherInfo;
		private Blob image;
		private String userName;
		private String password;
		
		public void copyValuesToBean(Person person){
			
			setId((int) person.getId());
			setFirstName(person.getFirstName());
			setLastName(person.getLastName());
			setEmail(person.getEmail());
			setPhoneNumber(person.getPhoneNumber());
			setOtherInfo(person.getOtherInfo());
			setImage(person.getImage());
			setUserName(person.getUserName());
			setPassword(person.getPassword());;
			
		}
		
		public void copyBeanValuesToPerson(Person person, Blob image){
			
			person.setId((int) getId());
			person.setFirstName(getFirstName());
			person.setLastName(getLastName());
			person.setEmail(getEmail());
			person.setPhoneNumber(getPhoneNumber());
			person.setOtherInfo(getOtherInfo());
			person.setImage(image);
			person.setUserName(getUserName());
			person.setPassword(getPassword());
			
		}
		
		
		public long getId() {
			return id;
		}
		public void setId(int id) {
			this.id = id;
		}
		public String getFirstName() {
			return firstName;
		}
		public void setFirstName(String firstName) {
			this.firstName = firstName;
		}
		public String getLastName() {
			return lastName;
		}
		public void setLastName(String lastName) {
			this.lastName = lastName;
		}
		public String getEmail() {
			return email;
		}
		public void setEmail(String email) {
			this.email = email;
		}
		public String getPhoneNumber() {
			return phoneNumber;
		}
		public void setPhoneNumber(String phoneNumber) {
			this.phoneNumber = phoneNumber;
		}
		public String getOtherInfo() {
			return otherInfo;
		}
		public void setOtherInfo(String otherInfo) {
			this.otherInfo = otherInfo;
		}

		public Blob getImage() {
			return image;
		}

		public void setImage(Blob image) {
			this.image = image;
		}

		public String getUserName() {
			return userName;
		}

		public void setUserName(String userName) {
			this.userName = userName;
		}

		public String getPassword() {
			return password;
		}

		public void setPassword(String password) {
			this.password = password;
		}

	}


