package se.lowdin.civilforsvaret.webapp.repository.impl;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import se.lowdin.civilforsvaret.webapp.Person;
import se.lowdin.civilforsvaret.webapp.repository.PersonRepository;


@Repository
public class PersonRepositoryJpa implements PersonRepository  {
	
	@Autowired
	private SessionFactory session;
	
	Logger log = Logger.getLogger(PersonRepositoryJpa.class);
	
	@Override
	public void createPerson(Person person) {
		session.getCurrentSession().save(person);
	}

	@Override
	public void editPerson(Person person) {
		session.getCurrentSession().update(person);
	}

	@Override
	public void deletePerson(int personId) {		
		session.getCurrentSession().delete(getPerson(personId));
	}

	@Override
	public Person getPerson(int personId) {
		return (Person)session.getCurrentSession().get(Person.class, personId);
	}

	@Override
	public List getAllPersons() {
		return session.getCurrentSession().createQuery("from Person").list();
	}
	
	
}