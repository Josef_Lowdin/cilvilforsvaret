package se.lowdin.civilforsvaret.webapp.repository;

import java.util.List;

import se.lowdin.civilforsvaret.webapp.Person;


public interface PersonRepository {
	
    void createPerson(Person person);
	
	void deletePerson(int id);
	
	void editPerson(Person person);
	
	List<Person>getAllPersons();
	
	Person getPerson(int id);

}
