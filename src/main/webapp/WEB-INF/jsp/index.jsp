<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    
    <%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Login</title>
<style type="text/css">
.myTable { background-color:#FFFFE0;border-collapse:collapse; }
.myTable th { background-color:#BDB76B;color:white; }
.myTable td, .myTable th { padding:5px;border:1px solid #BDB76B; }
</style>
</head>
<body>

<img src= "<%= request.getContextPath() %>/images/civil_logo.jpg"/>

<center>

<h1>Logga in</h1>

<form:form commandName="loginForm">

<table class="myTable">
<th> Användarnamn : </th><tr><td><form:input path="userName" /></td></tr><br>
</table>
<br><p>
<table class="myTable">
<th> Lösenord: </th><tr> <td><form:password path="password" /></tr></td>
 </table>
 

 <br>
 <input type="submit" value="OK" />
 
  </form:form>

</body>
</html>