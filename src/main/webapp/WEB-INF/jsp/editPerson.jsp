
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Redigera Person</title>
<style type="text/css">
.myTable {
	background-color: #FFFFE0;
	border-collapse: collapse;
}

.myTable th {
	background-color: #BDB76B;
	color: white;
}

.myTable td,.myTable th {
	padding: 5px;
	border: 1px solid #BDB76B;
}
</style>
</head>
<body>

	<img src="<%=request.getContextPath()%>/images/civil_logo.jpg" />

	<center>
		<form:form method="post" action="edit.html" commandName="editPersonBean" enctype="multipart/form-data">
          <form:errors path="*" cssClass="error"/>

			<table class="myTable">
					<c:if test="${editPersonBean.id > 0}">
					<tr>
						<th>ID</th>
						<td>${editPersonBean.id}</td>
					</tr>
				</c:if>	
				
				<tr>
					<td>F�rnamn</td>
					<td><form:input path="firstName" /></td>
				</tr>
				<tr>
					<td>Efternamn</td>
					<td><form:input path="lastName" /></td>

				</tr>
				<tr>
					<td>Telefon nummer</td>
					<td><form:input path="phoneNumber" /></td>
				</tr>
				<tr>
					<td>E-Mail</td>
					<td><form:input path="email" /></td>
				</tr>
				<tr>
					<td>�vrigt</td>
					<td><form:input path="otherInfo" /></td>
				</tr>

                <tr>
                    <td>Bild</td>
				    <td><form:label path ="image" />
					<input type="file" name="file" id="file" /> 
				</td>
				</tr>
				<tr>
					<td>Anv�ndarnamn</td>
					<td><form:input path="userName" /></td>
				</tr>
				<tr>
					<td>L�senord</td>
					<td><form:password path="password" /></td>
				</tr>
				<br>
				<br>
				<tr>
					<td><c:set var="submitText">
						OK
					</c:set> <input type="submit" size="20" value="${submitText}" /> <a
						href="<%=request.getContextPath()%>/person.html"></a></td>
					<td></td>
				</tr>
			</table>
		</form:form>
	</center>
	<br>

</body>
</html>