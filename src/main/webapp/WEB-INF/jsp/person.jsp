<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ include file="/WEB-INF/jsp/includes.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

<title>Member List</title>
<style type="text/css">
.myTable { background-color:white;border-collapse:collapse; }
.myTable th { background-color:#BDB76B;color:white; }
.myTable td, .myTable th { padding:5px;border:1px solid #BDB76B; width:800px; }
</style>
</head>
<body>

<img src= "<%= request.getContextPath() %>/images/civil_logo.jpg"/>

<center>

<h1>Medlemmar</h1>

<table class="myTable">

	<th>Bild</th>
	<th>F�rnamn</th>
	<th>Efternamn</th>
	<th>Telefon</th>
	<th>E-Post</th>
	<th>�vrigt</th>
	<th>Redigera</th>
	<th>Ta Bort</th>
	
	<c:forEach items="${personList}" var="person">
	 <tr>
	
			<td width = "100"><img src= "<%= request.getContextPath() %>/image/${person.id}.html" height ="120" width = "100"/></td>
			<td width = "200">${person.firstName}</td>
			<td>${person.lastName}</td>
			<td>${person.phoneNumber}</td>
			<td>${person.email}</td>
			<td width= "200">${person.otherInfo}</td>
			<td><center><a href = "<%= request.getContextPath() %>/editPerson/${person.id}.html"><img src= "<%= request.getContextPath() %>/images/edit.jpg" height ="35" width="35"/></center></a></td>
			<td><center><a href = "<%= request.getContextPath() %>/delete/${person.id}.html"><img src= "<%= request.getContextPath() %>/images/delete.jpg" height ="40" width="40"/></center></a></td>
		</tr>
	</c:forEach>
		
</table>
<br><br>
<table border = "0">
<tr>
			 <td>
				<a href ="<%= request.getContextPath() %>/createPerson.html"><img src="<%= request.getContextPath() %>/images/button.png"/></a> 
			</td>
		</tr>
	
</table>

</center>

</body>
</html>